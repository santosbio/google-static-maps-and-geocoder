<?php

/**
* 
* Classe para trabalhar alguns dados de geolocalização da API do Google Maps
*
* @author Adriano dos Santos <santosbio@gmail.com>
* @version  1.0 [05-08-2015 15:17]
* @package APIs Públicas\Google\Maps
* 
*/
class GMaps
{
	/**
	 * Realiza uma busca pelas coordenadas geográficas de uma cidade
	 * @param  String $cidade Nome da cidade.
	 * @param  String $uf     Estado.
	 * @param  String $region Abreviação do país, com 2 caracteres. Ex.: BR
	 * @return String         Retorna as coordenadas, separadas por vírgula.
	 */
	public function coordenadas_cidade($cidade, $uf, $region) {
		

		$cidade = str_replace(' ', '+', $cidade);

		// Busca na API do Google Maps
		$url="http://maps.googleapis.com/maps/api/geocode/json?address=".str_replace(' ', '+', $cidade)."+-+".str_replace(' ', '+', $uf)."&sensor=false&region=".$region;

		// Substitui espaços duplicados por simples
		$url = str_replace('++', '+', $url);

		// executa a URL via cURL
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_URL, $url);    // get the url contents
		$data = curl_exec($ch); // execute curl request
		curl_close($ch);

		$obj = json_decode($data);
		$results = $obj->results;

		foreach ($results as $r) {
			$geometry = $r->geometry;
		}

		$latitude = $geometry->location->lat;
		$longitude = $geometry->location->lng;

		$retorno = $latitude . ',' . $longitude;

		return $retorno;

	}

	/**
	 * Realiza uma busca pelas coordenadas geográficas de um endereço completo.
	 * @param  String $tipo_logradouro Ex.: Rua
	 * @param  String $logradouro      Ex.: Augusta
	 * @param  String $numero          Ex.: 2100
	 * @param  String $bairro          Ex.: Centro
	 * @param  String $cidade          Ex.: São Paulo
	 * @param  String $uf              Ex.: SP
	 * @param  String $region          Ex.: BR
	 * @return String                  Retorna as coordenadas, separadas por vírgula.
	 */
	public function coordenadas_endereco($tipo_logradouro, $logradouro, $numero, $bairro, $cidade, $uf, $region) {

		// Busca na API do Google Maps
		$url="http://maps.googleapis.com/maps/api/geocode/json?address=".str_replace(' ', '+', $tipo_logradouro).'+'.str_replace(' ', '+', $logradouro).',+'.str_replace(' ', '+', $numero).',+'.str_replace(' ', '+', $bairro).',+'.str_replace(' ', '+', $cidade)."+-+".str_replace(' ', '+', $uf)."&sensor=false&region=".$region;

		// Substitui espaços duplicados por simples
		$url = str_replace('++', '+', $url);

		// executa a URL via cURL
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_URL, $url);    // get the url contents
		$data = curl_exec($ch); // execute curl request
		curl_close($ch);

		$obj = json_decode($data);

		$results = $obj->results;

		foreach ($results as $r) {
			$geometry = $r->geometry;
		}

		$latitude = $geometry->location->lat;
		$longitude = $geometry->location->lng;

		$retorno = $latitude . ',' . $longitude;

		return $retorno;

	}

	/**
	 * Busca e salva na máquina por um mapa com o marcador do local pesquisado.
	 * @param  String $coords 		Coordenadas geográficas, separadas por vírgula. Ex.: -18.9146078,-48.2753801
	 * @param  String $tamX   		Largura, em pixels, do mapa. Ex.: 400
	 * @param  String $tamY   		Altura, em pixels, do mapa. Ex.: 200
	 * @param  String $diretorio   	Caminho relativo onde a imagem será salva. Ex.: teste/
	 * @return File           		Salva uma imagem no diretório. O diretório precisa existir e ter permissão de escrita.
	 */
	public function get_static_map($coords, $tamX, $tamY, $diretorio) {



		$nomeArquivo = str_replace('-', '', $coords);
		$nomeArquivo = str_replace('.', '', $nomeArquivo);
		$nomeArquivo = str_replace(',', '', $nomeArquivo);


		if (!file_exists($diretorio.$nomeArquivo.".png")) {
			
			$link = 'https://maps.googleapis.com/maps/api/staticmap?center='.$coords.'&zoom=15&size='.$tamX.'x'.$tamY.'&maptype=roadmap&markers=color:red%7Clabel:Destino%7C'.$coords;

			$image = file_get_contents($link); 
			$fp  = fopen($diretorio.$nomeArquivo.".png", "w+"); 

			fputs($fp, $image); 
			fclose($fp); 
			unset($image);
		} else {
			
		}
		
	}

}

?>
